package com.epam.taks.main.businessobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.support.FindBy;

import com.epam.taks.main.driver.WebDriverSetup;

import ru.yandex.qatools.htmlelements.element.Link;
import ru.yandex.qatools.htmlelements.element.TextInput;

public class YandexMarketPageObject extends MainYandexPageObject {

	private static WebDriver driver = WebDriverSetup.getWebDriver();

	@FindBy(xpath = "//*[text()='Электроника' and @class='link topmenu__link']")
	private Link electronicaLink;

	@FindBy(xpath = "(//*[text()='Мобильные телефоны'])[2]")
	private Link mobileLink;

	@FindBy(xpath = "(//*[text()='Наушники и Bluetooth-гарнитуры'])[1]")
	private Link headphonesLink;
	
	@FindBy(xpath = "(//*[text()='Ещё'])[2]")
	private Link expandLnk;

	@FindBy(xpath = "//*[@name='Цена от']")
	private TextInput priceFrom;

	@FindBy(xpath = "//*[@name='Цена до']")
	private TextInput priceTo;

	public YandexMarketPageObject() {
		super();
	}

	public void goToMobile() throws InterruptedException {
		electronicaLink.click();
		mobileLink.click();
	}

	public void goToHeadphones() throws InterruptedException, ElementNotVisibleException {
		electronicaLink.click();
		expandLnk.click();
		headphonesLink.click();
	}

	public void setFilter(String name, String startPrice, String endPrice) throws InterruptedException {

		if (name != null) {
			driver.findElement(By.xpath("//*[@class='LhMupC0dLR']/*[text()='" + name + "']")).click();
		}

		if (startPrice != null) {
			priceFrom.clear();
			priceFrom.sendKeys(startPrice);
		}

		if (endPrice != null) {
			priceTo.clear();
			priceTo.sendKeys(endPrice);
		}

		Thread.sleep(3000);
	}

	public String getNameByNumber(String number) {
		return driver.findElement(By.xpath("(//*[@class='n-snippet-cell2__title'])[" + number + "]")).getText();
	}

	public void checkByNumber(String number) throws WebDriverException {
		driver.findElement(By.xpath("(//*[@class='image'])[" + number + "]")).click();
	}

	public String getTitle() {
		return driver.findElement(By.xpath("//h1")).getText();
	}
}
