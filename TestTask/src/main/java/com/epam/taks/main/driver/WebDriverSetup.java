package com.epam.taks.main.driver;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.ChromeDriverManager;

public class WebDriverSetup {

	public static ChromeDriver driver;

	public static WebDriver getWebDriver() {
		if (driver == null) {
			ChromeDriverManager.getInstance().setup();
			driver = new ChromeDriver();
			driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		}
		return driver;
	}
}
