package com.epam.taks.main.businessobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

import com.epam.taks.main.driver.WebDriverSetup;

import ru.yandex.qatools.htmlelements.element.Link;
import ru.yandex.qatools.htmlelements.element.TextInput;

public class EpamPageObject extends MainYandexPageObject {

	private static WebDriver driver = WebDriverSetup.getWebDriver();

	@FindBy(xpath = "//*[@href='/careers']")
	private WebElement carierLink;

	@FindBy(xpath = "//*[@href='/careers/job-listings']")
	private WebElement vacancyLink;

	@FindBy(xpath = "//*[@class='job-search-input']")
	private TextInput linkinWords;

	@FindBy(xpath = "(//*[@class='button-apply']/a)[1]")
	private Link firstVacancy;

	public EpamPageObject() {
		super();
	}

	public void navigateToVacancy() {
		new Actions(WebDriverSetup.getWebDriver()).moveToElement(carierLink).moveToElement(vacancyLink).click().build()
				.perform();
	}

	public void setLinkinWords(String words) throws InterruptedException {
		linkinWords.clear();
		linkinWords.sendKeys(words);
		new Actions(driver).sendKeys(Keys.ENTER).build().perform();
		Thread.sleep(3000);
	}

	public void clickVacancy() {
		firstVacancy.click();
	}

	public String getDescription() {
		return driver.findElement(By.xpath("//*[@class='recruiting-details-description']")).getText();
	}
}
