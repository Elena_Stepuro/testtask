package com.epam.taks.main.businessobjects;

import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.epam.taks.main.driver.WebDriverSetup;

import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.Link;
import ru.yandex.qatools.htmlelements.element.TextInput;
import ru.yandex.qatools.htmlelements.loader.decorator.HtmlElementDecorator;

public class MainYandexPageObject {

	private static WebDriver driver = WebDriverSetup.getWebDriver();

	@FindBy(xpath = "//*[@data-statlog='tabs.market']")
	private Link marketLink;

	@FindBy(xpath = "//*[@aria-label='Запрос']")
	private TextInput searchText;

	@FindBy(xpath = "//*[@type='submit']")
	private Button searchButton;

	public MainYandexPageObject() {
		PageFactory.initElements(new HtmlElementDecorator(driver), this);
	}

	public YandexMarketPageObject goToMarket() {
		marketLink.click();

		return new YandexMarketPageObject();
	}

	public void findByText(String text) {
		searchText.clear();
		searchText.sendKeys(text);
		searchButton.click();
	}

	public EpamPageObject goToEpamFronSearch() {
		driver.findElement(By.xpath("(//*[@class='organic__url-text'])[1]")).click();
		ArrayList<String> availableWindows = new ArrayList<String>(WebDriverSetup.getWebDriver().getWindowHandles());
		if (!availableWindows.isEmpty()) {
			WebDriverSetup.getWebDriver().switchTo().window(availableWindows.get(1));
		}
		return new EpamPageObject();
	}
}
