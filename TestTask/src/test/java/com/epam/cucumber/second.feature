Feature: SecondTask

Scenario: SecondTask
	Given I can navigate to "http://yandex.ru"
	And I can go to "Market"
	And I can choose "Mobile"
	And I can set filter "Samsung" and price from "null" to "40000"
	And I can get "1" name
	And I can go to page "1"
	
	And I can navigate to "http://yandex.ru"
	And I can go to "Market"
	And I can choose "Headphones"
	And I can set filter "Beats" and price from "17000" to "25000"
	And I can get "1" name
	And I can go to page "1"