package com.epam.tests.steps;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import com.epam.taks.main.businessobjects.EpamPageObject;
import com.epam.taks.main.driver.WebDriverSetup;

import cucumber.api.CucumberOptions;
import cucumber.api.java.en.Given;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = "/TestTask/src/test/java/com/epam/cucumber/third.feature")
public class ThirdTaskStep {

	private static final String VACANCY_PAGE = "https://www.epam-group.ru/careers/job-listings";
	private EpamPageObject epamPageObject;
	private WebDriver driver = WebDriverSetup.getWebDriver();

	private String name = null;
	private String driverName = driver.toString().split(":")[0];
	private String dateTime = LocalDateTime.now().toString().replaceAll(":", ".");

	public ThirdTaskStep() {
		epamPageObject = new EpamPageObject();
	}

	@Given("^I can go to section \"([^\"]*)\"$")
	public void goToSection(String section) {
		epamPageObject.navigateToVacancy();
		Assert.assertEquals(driver.getCurrentUrl(), VACANCY_PAGE);
	}

	@Given("^I can set linkin words \"([^\"]*)\"$")
	public void setLinkinWords(String linkinWords) throws InterruptedException {
		epamPageObject.setLinkinWords(linkinWords);
		epamPageObject.clickVacancy();
	}

	@Given("^I can check description using \"([^\"]*)\"$")
	public void getText(String searcher) throws IOException {
		name = epamPageObject.getDescription();
		
		BufferedWriter bufWriter = new BufferedWriter(
				  new OutputStreamWriter(
				    new FileOutputStream(dateTime + "_" + driverName + "_" + searcher + ".txt"),
				    Charset.forName("UTF-8")
				    )
				  );
		bufWriter.write(name);
		bufWriter.flush();
		
		List<String> expected=new ArrayList<>();
		Files.lines(Paths.get("expected.txt"), StandardCharsets.UTF_8).forEach(x->{expected.add(x);});
		
		Assert.assertTrue(name.contains(expected.get(1)));
	}
}
