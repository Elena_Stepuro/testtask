package com.epam.tests.steps;

import org.junit.runner.RunWith;

import com.epam.taks.main.businessobjects.YandexMarketPageObject;
import com.epam.taks.main.driver.WebDriverSetup;

import cucumber.api.CucumberOptions;
import cucumber.api.java.en.Given;
import cucumber.api.junit.Cucumber;

import org.junit.AfterClass;
import org.junit.Assert;

@RunWith(Cucumber.class)
@CucumberOptions(features = "/TestTask/src/test/java/com/epam/cucumber/second.feature")
public class SecondTaskStep {

	private YandexMarketPageObject yandexMarketPageObject;

	private String name = null;

	public SecondTaskStep() {
		yandexMarketPageObject = new YandexMarketPageObject();
	}

	@Given("^I can choose \"([^\"]*)\"$")
	public void choose(String link) throws InterruptedException {
		if (link.equals("Mobile")) {
			yandexMarketPageObject.goToMobile();
		}

		if (link.equals("Headphones")) {
			yandexMarketPageObject.goToHeadphones();
		}
	}

	@Given("^I can set filter \"([^\"]*)\" and price from \"([^\"]*)\" to \"([^\"]*)\"$")
	public void setFilter(String model, String startPrice, String endPrice) throws InterruptedException {
		yandexMarketPageObject.setFilter(model, startPrice, endPrice);
	}

	@Given("^I can get \"([^\"]*)\" name$")
	public void getName(String number) {
		name = yandexMarketPageObject.getNameByNumber(number);
	}

	@Given("^I can go to page \"([^\"]*)\"$")
	public void goToPage(String number) {
		yandexMarketPageObject.checkByNumber(number);
		String title = yandexMarketPageObject.getTitle();
		Assert.assertEquals(name, title);
	}

	@AfterClass
	public void cleanup() {
		WebDriverSetup.getWebDriver().quit();
	}
}
