package com.epam.tests.steps;

import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import com.epam.taks.main.businessobjects.MainYandexPageObject;
import com.epam.taks.main.driver.WebDriverSetup;

import cucumber.api.CucumberOptions;
import cucumber.api.java.en.Given;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = { "/TestTask/src/test/java/com/epam/cucumber/second.feature",
		"/TestTask/src/test/java/com/epam/cucumber/third.feature" })
public class MainSteps {

	private MainYandexPageObject mainYandexPageObject;
	private WebDriver driver = WebDriverSetup.getWebDriver();

	public MainSteps() {
		mainYandexPageObject = new MainYandexPageObject();
	}

	@Given("^I can navigate to \"([^\"]*)\"$")
	public void navigateTo(String URL) {
		driver.navigate().to(URL);
		driver.manage().window().maximize();
	}

	@Given("^I can go to \"([^\"]*)\"$")
	public void goTo(String link) throws InterruptedException {

		if (link.equals("Market")) {
			mainYandexPageObject.goToMarket();
		}

		if (link.equals("EPAM systems")) {
			mainYandexPageObject.findByText(link);
			mainYandexPageObject.goToEpamFronSearch();
		}

		System.setProperty("console.encoding","Cp866");
	}
}
