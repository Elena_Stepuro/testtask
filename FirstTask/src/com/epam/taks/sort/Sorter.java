package com.epam.taks.sort;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Sorter {

	private static final String FILE_PATH = "file.csv";

	public static void main(String[] args) throws IOException {
		generateNumbersInFile();

		List<Integer> numbers = readFromFile();

		numbers = sortAscending(numbers);
		System.out.println(numbers.toString());

		numbers = sortDescending(numbers);
		System.out.println(numbers.toString());

	}

	/**
	 * Pre-condition step
	 * 
	 * @throws IOException
	 */
	private static void generateNumbersInFile() throws IOException {
		List<Integer> numbers = generateNumbers();
		writeIntoFile(numbers);
	}

	/**
	 * Generate numbers from 0 to 20 and mix them in random
	 * 
	 * @return list of random numbers from 0 to 20
	 */
	private static List<Integer> generateNumbers() {
		List<Integer> numbers = new ArrayList<>();
		for (int i = 0; i < 20; i++) {
			numbers.add(i);
		}
		Collections.shuffle(numbers);
		return numbers;
	}

	/**
	 * Sort list of numbers in descending
	 * 
	 * @param numbers
	 * @return list of numbers sort in descending
	 */
	private static List<Integer> sortDescending(List<Integer> numbers) {
		sortAscending(numbers);
		Collections.reverse(numbers);
		return numbers;
	}

	/**
	 * Sort list of numbers in ascending
	 * 
	 * @param numbers
	 * @return list of numbers sort in ascending
	 */
	private static List<Integer> sortAscending(List<Integer> numbers) {
		Collections.sort(numbers);
		return numbers;
	}

	/**
	 * Write list of numbers into csv file
	 * 
	 * @param numbers
	 * @throws IOException
	 */
	private static void writeIntoFile(List<Integer> numbers) throws IOException {

		FileWriter writer = new FileWriter(FILE_PATH);
		numbers.stream().forEach(x -> {
			try {
				writer.write(x.toString());
				writer.write('\n');
			} catch (IOException e) {
				e.printStackTrace();
			}
		});

		writer.flush();
	}

	/**
	 * Read lines from csv file and add them into list
	 * 
	 * @return
	 * @throws IOException
	 */
	private static List<Integer> readFromFile() throws IOException {
		List<Integer> numbers = new ArrayList<>();

		Files.lines(Paths.get(FILE_PATH), StandardCharsets.UTF_8).forEach(x -> {
			numbers.add(Integer.parseInt(x));
		});

		return numbers;
	}

}
